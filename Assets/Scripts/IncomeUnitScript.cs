using System;
using Assets.Scripts;
using UnityEngine;

public class IncomeUnitScript : MonoBehaviour
{
    public UnitTier UnitTier;
    public PlayerRace Race;

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
}

/// <summary>
/// Store income unit default values.
/// </summary>
public class UnitDto
{
    public UnitTier Tier { get; private set; }

    public PlayerRace Race { get; private set; }

    public string Name { get; private set; }

    public UnlimitedValue BaseCost { get; private set; }

    /// <summary>
    /// Cost increase in % for next unit.
    /// </summary>
    public double NextUnitIncrease { get; private set; }

    public UnlimitedValue Income { get; private set; }
}
