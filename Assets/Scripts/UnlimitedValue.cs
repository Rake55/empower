using System;

namespace Assets.Scripts
{
    /// <summary>
    /// Represent really big number.
    /// </summary>
    public sealed class UnlimitedValue
    {
        private string _asString;
        private bool _asStringNeedUpdate = true;

        public UnlimitedValue()
        {
            Value = 0;
            Power = 0;
        }

        public UnlimitedValue(decimal value, int power)
        {
            if(value >= 10) throw new ArgumentOutOfRangeException("value", string.Format("Value should be lesser than 10, actual value is {0}", value));
            if(value < 1) throw new ArgumentOutOfRangeException("value", string.Format("Value should be greater than 1, actual value is {0}", value));

            Value = value;
            Power = power;
        }

        /// <summary>
        /// Mean value.
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// 10 ^ Power * Value = real value.
        /// </summary>
        public int Power { get; set; }

        #region Sum
        /// <summary>
        /// Math operation.
        /// </summary>
        public void Sum(double other)
        {
            Sum(Convert.ToDecimal(other), 0);
        }


        private void Sum(decimal value, int power)
        {
            if(Math.Abs(Power - power) > 25)
                return;

            if (Power > power)
            {
                var basePowerValue = ValueToBasePower(value, Power, power) + Value;

                CorrectAndUpdateValue(basePowerValue, Power);
            }
            if (power < Power)
            {
                var basePowerValue = ValueToBasePower(value, power, Power) + Value;

                CorrectAndUpdateValue(basePowerValue, power);
            }
        }
        #endregion

        #region Divide
        /// <summary>
        /// Math operation.
        /// </summary>
        public void Divide(double other)
        {
            Divide(Convert.ToDecimal(other), 0);
        }


        /// <summary>
        /// Math operation.
        /// </summary>
        public void Divide(decimal other)
        {
            Divide(other, 0);
        }

        /// <summary>
        /// Math operation.
        /// </summary>
        public void Divide(int other)
        {
            Divide(other, 0);
        }

        /// <summary>
        /// Math operation.
        /// </summary>
        public void Divide(UnlimitedValue other)
        {
            if (other == null) throw new ArgumentNullException("other");

            Divide(other.Value, other.Power);
        }


        private void Divide(decimal value, int power)
        {
            if (value == 0) throw new ArgumentOutOfRangeException("value", "division by zero");

            var newValue = Value / value;

            var newPower = Power - power;

            CorrectAndUpdateValue(newValue, newPower);
        }
        #endregion

        #region Multiply
        /// <summary>
        /// Math operation.
        /// </summary>
        public void Multiply(double other)
        {
            Multiply(Convert.ToDecimal(other), 0);
        }

        /// <summary>
        /// Math operation.
        /// </summary>
        public void Multiply(decimal other)
        {
            Multiply(other, 0);
        }

        /// <summary>
        /// Math operation.
        /// </summary>
        public void Multiply(int other)
        {
            Multiply(other, 0);
        }

        /// <summary>
        /// Math operation.
        /// </summary>
        public void Multiply(UnlimitedValue other)
        {
            if (other == null) throw new ArgumentNullException("other");

            Multiply(other.Value, other.Power);
        }


        private void Multiply(decimal value, int power)
        {
            var newValue = Value * value;

            var newPower = Power + power;

            CorrectAndUpdateValue(newValue, newPower);
        }
        #endregion

        private void CorrectAndUpdateValue(decimal newValue, int newPower)
        {
            while (newValue >= 10)
            {
                newValue = newValue / 10;
                newPower += 1;
            }
            while (newValue < 1)
            {
                newValue = newValue * 10;
                newPower -= 1;
            }

            Value = newValue;
            Power = newPower;
            _asStringNeedUpdate = true;
        }

        private decimal ValueToBasePower(decimal value, int bigPower, int smallPower)
        {
            var powerDelta = bigPower - smallPower;

            while (powerDelta > 0)
            {
                value = value / 10;
                powerDelta--;
            }

            return value;
        }

        #region Overrides
        public override string ToString()
        {
            if (!_asStringNeedUpdate)
                return _asString;

            _asString = PresentAsString();
            _asStringNeedUpdate = false;
            return _asString;
        }

        private string PresentAsString()
        {
            return Power > -1 && Power < 4
                ? (Value*(decimal) Math.Pow(10, Power)).ToString("0.##")
                : string.Format("{0:0.##}e{1}", Value, Power);
        }

        public override bool Equals(object other)
        {
            return Equals(other as UnlimitedValue);
        }

        public bool Equals(UnlimitedValue other)
        {
            if (other == null) { return false; }
            if (ReferenceEquals(this, other)) { return true; }
            return Power == other.Power && Value == other.Value;
        }

        public static bool operator ==(UnlimitedValue item1, UnlimitedValue item2)
        {
            if (ReferenceEquals(item1, item2)) { return true; }
            if ((object)item1 == null || (object)item2 == null) { return false; }
            return item1.Power == item2.Power && item1.Value == item2.Value;
        }

        public static bool operator !=(UnlimitedValue item1, UnlimitedValue item2)
        {
            return !(item1 == item2);
        }

        public override int GetHashCode()
        {
            return (Power << 16) ^ Value.GetHashCode();
        }
        #endregion
    }
}
