using System;
using UnityEngine;

public class GlobalConstants : MonoBehaviour
{
    public const decimal DefaultPriceIncrease = 1.5M;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

/// <summary>
/// Represent unit type that earn gold.
/// </summary>
public enum UnitTier
{
    Tier1 = 1,
    Tier2 = 2,
    Tier3 = 3,
    Tier4 = 4,
    Tier5 = 5,
    Tier6 = 6,
    Tier7 = 7,
}

public enum PlayerRace
{
    Human = 0
}
