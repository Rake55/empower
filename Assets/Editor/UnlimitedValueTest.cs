using System.Diagnostics;
using Assets.Scripts;
using NUnit.Framework;

namespace Assets.Editor
{
    public class UnlimitedValueTest {

        [Test]
        public void MultiplyTest()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            var multiply = new UnlimitedValue(1, 1);
            var expected = new UnlimitedValue(5.122354M, 5);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void Multiply2Test()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            const int multiply = 100;
            var expected = new UnlimitedValue(5.122354M, 6);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void MultiplyTest3()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            var multiply = new UnlimitedValue(1.23M, 44);
            var expected = new UnlimitedValue(6.30049542M, 48);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void Multiply4Test()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            const decimal multiply = 1000M;
            var expected = new UnlimitedValue(5.122354M, 7);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void Multiply5Test()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            const double multiply = 10000;
            var expected = new UnlimitedValue(5.122354M, 8);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void Multiply6Test()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            const double multiply = 0.01;
            var expected = new UnlimitedValue(5.122354M, 2);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void MultiplyTest7()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            var multiply = new UnlimitedValue(1, -10);
            var expected = new UnlimitedValue(5.122354M, -6);

            target.Multiply(multiply);

            Assert.AreEqual(expected, target);
        }

        [Test]
        public void ToStringTest()
        {
            var target = new UnlimitedValue(5.122354M, 4);
            const string expected = "5.12e4";

            Assert.AreEqual(expected, target.ToString());
        }

        [Test]
        public void DivideTest1()
        {
            var target = new UnlimitedValue(1.563M, 4);
            var divide = new UnlimitedValue(1.3M, 2);
            var expected = new UnlimitedValue(1.2023076923076923076923076923M, 2);

            target.Divide(divide);
            Assert.AreEqual(expected, target);
        }

        [Test]
        public void DivideTest2()
        {
            var target = new UnlimitedValue(1M, 1);
            var divide = new UnlimitedValue(2M, 0);
            var expected = new UnlimitedValue(5M, 0);

            target.Divide(divide);
            Assert.AreEqual(expected, target);
        }

        [Test]
        public void DivideTest3()
        {
            var target = new UnlimitedValue(1M, 1);
            var divide = new UnlimitedValue(2M, 200);
            var expected = new UnlimitedValue(5M, -200);

            target.Divide(divide);
            Assert.AreEqual(expected, target);
        }
    }
}
